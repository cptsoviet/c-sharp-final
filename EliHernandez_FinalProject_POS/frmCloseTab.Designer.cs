﻿namespace EliHernandez_FinalProject_POS
{
    partial class frmCloseTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxPayButtons = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.radCreditDebit = new System.Windows.Forms.RadioButton();
            this.radCash = new System.Windows.Forms.RadioButton();
            this.txtTipAmount = new System.Windows.Forms.TextBox();
            this.gbxCreditDebitPayment = new System.Windows.Forms.GroupBox();
            this.btnGoBack = new System.Windows.Forms.Button();
            this.txtCVCnumber = new System.Windows.Forms.TextBox();
            this.lblSecurityNumber = new System.Windows.Forms.Label();
            this.txtCardExpiration = new System.Windows.Forms.TextBox();
            this.lblExpDate = new System.Windows.Forms.Label();
            this.btnEnter = new System.Windows.Forms.Button();
            this.txtCreditDebitCardNumber = new System.Windows.Forms.TextBox();
            this.lblCreditDebit = new System.Windows.Forms.Label();
            this.txtCloseTabTotal = new System.Windows.Forms.TextBox();
            this.lblTip = new System.Windows.Forms.Label();
            this.gbxPayButtons.SuspendLayout();
            this.gbxCreditDebitPayment.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxPayButtons
            // 
            this.gbxPayButtons.Controls.Add(this.btnSelect);
            this.gbxPayButtons.Controls.Add(this.radCreditDebit);
            this.gbxPayButtons.Controls.Add(this.radCash);
            this.gbxPayButtons.Location = new System.Drawing.Point(103, 307);
            this.gbxPayButtons.Name = "gbxPayButtons";
            this.gbxPayButtons.Size = new System.Drawing.Size(144, 105);
            this.gbxPayButtons.TabIndex = 3;
            this.gbxPayButtons.TabStop = false;
            this.gbxPayButtons.Text = "Pay";
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(37, 69);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(75, 23);
            this.btnSelect.TabIndex = 3;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // radCreditDebit
            // 
            this.radCreditDebit.AutoSize = true;
            this.radCreditDebit.Location = new System.Drawing.Point(6, 46);
            this.radCreditDebit.Name = "radCreditDebit";
            this.radCreditDebit.Size = new System.Drawing.Size(82, 17);
            this.radCreditDebit.TabIndex = 1;
            this.radCreditDebit.TabStop = true;
            this.radCreditDebit.Text = "Credit/Debit";
            this.radCreditDebit.UseVisualStyleBackColor = true;
            // 
            // radCash
            // 
            this.radCash.AutoSize = true;
            this.radCash.Location = new System.Drawing.Point(6, 19);
            this.radCash.Name = "radCash";
            this.radCash.Size = new System.Drawing.Size(49, 17);
            this.radCash.TabIndex = 0;
            this.radCash.TabStop = true;
            this.radCash.Text = "Cash";
            this.radCash.UseVisualStyleBackColor = true;
            // 
            // txtTipAmount
            // 
            this.txtTipAmount.Location = new System.Drawing.Point(156, 284);
            this.txtTipAmount.Name = "txtTipAmount";
            this.txtTipAmount.Size = new System.Drawing.Size(76, 20);
            this.txtTipAmount.TabIndex = 5;
            this.txtTipAmount.Leave += new System.EventHandler(this.txtTipAmount_Leave);
            // 
            // gbxCreditDebitPayment
            // 
            this.gbxCreditDebitPayment.Controls.Add(this.btnGoBack);
            this.gbxCreditDebitPayment.Controls.Add(this.txtCVCnumber);
            this.gbxCreditDebitPayment.Controls.Add(this.lblSecurityNumber);
            this.gbxCreditDebitPayment.Controls.Add(this.txtCardExpiration);
            this.gbxCreditDebitPayment.Controls.Add(this.lblExpDate);
            this.gbxCreditDebitPayment.Controls.Add(this.btnEnter);
            this.gbxCreditDebitPayment.Controls.Add(this.txtCreditDebitCardNumber);
            this.gbxCreditDebitPayment.Controls.Add(this.lblCreditDebit);
            this.gbxCreditDebitPayment.Location = new System.Drawing.Point(366, 8);
            this.gbxCreditDebitPayment.Name = "gbxCreditDebitPayment";
            this.gbxCreditDebitPayment.Size = new System.Drawing.Size(241, 259);
            this.gbxCreditDebitPayment.TabIndex = 6;
            this.gbxCreditDebitPayment.TabStop = false;
            this.gbxCreditDebitPayment.Text = "Credit/Debit";
            this.gbxCreditDebitPayment.Visible = false;
            // 
            // btnGoBack
            // 
            this.btnGoBack.Location = new System.Drawing.Point(22, 198);
            this.btnGoBack.Name = "btnGoBack";
            this.btnGoBack.Size = new System.Drawing.Size(75, 23);
            this.btnGoBack.TabIndex = 8;
            this.btnGoBack.Text = "Go Back";
            this.btnGoBack.UseVisualStyleBackColor = true;
            this.btnGoBack.Click += new System.EventHandler(this.btnGoBack_Click);
            // 
            // txtCVCnumber
            // 
            this.txtCVCnumber.Location = new System.Drawing.Point(82, 151);
            this.txtCVCnumber.Name = "txtCVCnumber";
            this.txtCVCnumber.Size = new System.Drawing.Size(78, 20);
            this.txtCVCnumber.TabIndex = 3;
            // 
            // lblSecurityNumber
            // 
            this.lblSecurityNumber.AutoSize = true;
            this.lblSecurityNumber.Location = new System.Drawing.Point(19, 130);
            this.lblSecurityNumber.Name = "lblSecurityNumber";
            this.lblSecurityNumber.Size = new System.Drawing.Size(201, 13);
            this.lblSecurityNumber.TabIndex = 7;
            this.lblSecurityNumber.Text = "Enter CVC code in the back of your card:";
            this.lblSecurityNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCardExpiration
            // 
            this.txtCardExpiration.Location = new System.Drawing.Point(68, 101);
            this.txtCardExpiration.Name = "txtCardExpiration";
            this.txtCardExpiration.Size = new System.Drawing.Size(109, 20);
            this.txtCardExpiration.TabIndex = 2;
            // 
            // lblExpDate
            // 
            this.lblExpDate.AutoSize = true;
            this.lblExpDate.Location = new System.Drawing.Point(47, 76);
            this.lblExpDate.Name = "lblExpDate";
            this.lblExpDate.Size = new System.Drawing.Size(148, 13);
            this.lblExpDate.TabIndex = 5;
            this.lblExpDate.Text = "Enter expiration date (MMYY):";
            this.lblExpDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnEnter
            // 
            this.btnEnter.Location = new System.Drawing.Point(133, 198);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(75, 23);
            this.btnEnter.TabIndex = 4;
            this.btnEnter.Text = "Enter";
            this.btnEnter.UseVisualStyleBackColor = true;
            this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // txtCreditDebitCardNumber
            // 
            this.txtCreditDebitCardNumber.Location = new System.Drawing.Point(6, 42);
            this.txtCreditDebitCardNumber.Name = "txtCreditDebitCardNumber";
            this.txtCreditDebitCardNumber.Size = new System.Drawing.Size(229, 20);
            this.txtCreditDebitCardNumber.TabIndex = 1;
            // 
            // lblCreditDebit
            // 
            this.lblCreditDebit.AutoSize = true;
            this.lblCreditDebit.Location = new System.Drawing.Point(23, 16);
            this.lblCreditDebit.Name = "lblCreditDebit";
            this.lblCreditDebit.Size = new System.Drawing.Size(185, 13);
            this.lblCreditDebit.TabIndex = 0;
            this.lblCreditDebit.Text = "Swipe card or manually type numbers:";
            this.lblCreditDebit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCloseTabTotal
            // 
            this.txtCloseTabTotal.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCloseTabTotal.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtCloseTabTotal.Location = new System.Drawing.Point(12, 8);
            this.txtCloseTabTotal.Multiline = true;
            this.txtCloseTabTotal.Name = "txtCloseTabTotal";
            this.txtCloseTabTotal.ReadOnly = true;
            this.txtCloseTabTotal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCloseTabTotal.Size = new System.Drawing.Size(339, 259);
            this.txtCloseTabTotal.TabIndex = 7;
            // 
            // lblTip
            // 
            this.lblTip.AutoSize = true;
            this.lblTip.Location = new System.Drawing.Point(106, 287);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(25, 13);
            this.lblTip.TabIndex = 8;
            this.lblTip.Text = "Tip:";
            // 
            // frmCloseTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 435);
            this.Controls.Add(this.lblTip);
            this.Controls.Add(this.txtCloseTabTotal);
            this.Controls.Add(this.gbxCreditDebitPayment);
            this.Controls.Add(this.txtTipAmount);
            this.Controls.Add(this.gbxPayButtons);
            this.Name = "frmCloseTab";
            this.Text = "Close Tab";
            this.Load += new System.EventHandler(this.frmCloseTab_Load);
            this.gbxPayButtons.ResumeLayout(false);
            this.gbxPayButtons.PerformLayout();
            this.gbxCreditDebitPayment.ResumeLayout(false);
            this.gbxCreditDebitPayment.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox gbxPayButtons;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.RadioButton radCreditDebit;
        private System.Windows.Forms.RadioButton radCash;
        private System.Windows.Forms.TextBox txtTipAmount;
        private System.Windows.Forms.GroupBox gbxCreditDebitPayment;
        private System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.TextBox txtCreditDebitCardNumber;
        private System.Windows.Forms.Label lblCreditDebit;
        private System.Windows.Forms.TextBox txtCVCnumber;
        private System.Windows.Forms.Label lblSecurityNumber;
        private System.Windows.Forms.TextBox txtCardExpiration;
        private System.Windows.Forms.Label lblExpDate;
        private System.Windows.Forms.Button btnGoBack;
        private System.Windows.Forms.TextBox txtCloseTabTotal;
        private System.Windows.Forms.Label lblTip;
    }
}