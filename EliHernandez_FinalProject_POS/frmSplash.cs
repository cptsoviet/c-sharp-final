﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EliHernandez_FinalProject_POS
{
    public partial class frmSplash : Form
    {
        private int counter = 5;
        public frmSplash()
        {
            InitializeComponent();
        }

        private void frmSplash_Load(object sender, EventArgs e)
        {
            //To Maximize the size of the form
            this.WindowState = FormWindowState.Maximized;
            tmrCountdown.Start();
        }

        private void tmrCountdown_Tick(object sender, EventArgs e)
        {
            counter--;
            lblCounter.Text = counter.ToString();
        }
        
        private void tmrCloseForm_Tick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
