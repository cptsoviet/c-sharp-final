﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EliHernandez_FinalProject_POS
{
    public partial class frmCloseTab : Form
    {
        const string RECEIPT_DIR = "\\sales\\"; // directory where order receipts are stored
        const string ORDER_COUNT_FILE = "lastOrderNo.txt"; // name of text text file where last order # is stored
        StreamWriter sw;
        StreamReader sr;

        const decimal TAX_RATE = 0.095m;
        List<Item> orderList;
        int orderNo;
        string userName;
        decimal subtotal = 0;
        string receiptText = "";
        decimal tax;
        decimal tip = 0;
        decimal total;

        string dateTime = "";

        //const for card verification
        const int CARD_NUMBER_LENGTH = 16, EXP_DATE_LENGTH = 4, CVC_NUMBER_LENGTH = 3;

        //variables to hold user input for card
        int expDate, cvcNumber;
        ulong cardNumber;

        // Constructor receives username and orderList as arguments
        public frmCloseTab(string userName, List<Item> order)
        {
            InitializeComponent();
            orderList = order;
            this.userName = userName;
        }

        // Payment Options
        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (radCash.Checked == true)
            {
                DialogResult result = MessageBox.Show("Pay with cash?", "Cash Payment", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    //disable buttons so user cannot input junk
                    txtTipAmount.Enabled = false;
                    radCreditDebit.Enabled = false;
                    btnSelect.Enabled = false;

                    MessageBox.Show("Your server will be with you to collect payment. Thank you for dining with us, " + userName);

                    saveReceipt();

                    Application.Restart(); //To restart program for new customer
                }                
            }

            if(radCreditDebit.Checked==true)
            {
                DialogResult result = MessageBox.Show("Pay with card?", "Card Payment", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    // Resize form to display CC entry options
                    Size wide = new Size(634, 474);
                    this.Size = wide;
                    radCash.Enabled = false;
                    gbxCreditDebitPayment.Visible = true;
                    txtCreditDebitCardNumber.Focus();
                    txtTipAmount.Enabled = false;
                    btnSelect.Enabled = false;
                }
            }
        }

        // in case user makes another wrong selection, user can go back
        private void btnGoBack_Click(object sender, EventArgs e)
        {
            gbxCreditDebitPayment.Visible = false;
            txtTipAmount.Enabled = true;
            radCash.Enabled = true;
            btnSelect.Enabled = true;
        }

        // Tip amount updates when user switches from tip textbox
        private void txtTipAmount_Leave(object sender, EventArgs e)
        {
            // if tip field contains text
            if (txtTipAmount.Text != "")
            {
                try
                {
                    // Parse tip as decimal.
                    tip = decimal.Parse(txtTipAmount.Text);
                }
                catch
                {
                    MessageBox.Show("Please enter a dollar amount for tip.");
                    txtTipAmount.Text = "";
                    txtTipAmount.Focus();
                }
            }
            else // if user hasn't entered a tip it defaults to $0.
            {
                tip = 0;
            }

            printReceipt(); // updates the receipt to include tip
        }

        //button for processing credit and debit payments
        //with input validation
        private void btnEnter_Click(object sender, EventArgs e)
        {
            if (txtCreditDebitCardNumber.Text.Length < CARD_NUMBER_LENGTH||txtCreditDebitCardNumber.Text.Length>CARD_NUMBER_LENGTH)
            {
                MessageBox.Show("Please enter the correct number of digits","Card Number 16 digits");
                txtCreditDebitCardNumber.Focus();      
            }
            else if(!UInt64.TryParse(txtCreditDebitCardNumber.Text, out cardNumber))
            {
                MessageBox.Show("Please make sure you enter only numbers","Card Number Error: non-numbers");
                txtCreditDebitCardNumber.Focus();
            }
            else if (txtCardExpiration.Text.Length<EXP_DATE_LENGTH||txtCardExpiration.Text.Length>EXP_DATE_LENGTH)
            {
                MessageBox.Show("Please enter the correct number of digits MMYY","Card Expiration 4 digits");
                txtCardExpiration.Focus();        
            }
            else if (!int.TryParse(txtCardExpiration.Text, out expDate))
            {
                MessageBox.Show("Please make sure you enter only numbers","Exp Date Error: non-numbers");
                txtCardExpiration.Focus();
            }
            else if (txtCVCnumber.Text.Length < CVC_NUMBER_LENGTH || txtCVCnumber.Text.Length > CVC_NUMBER_LENGTH)
            {
                MessageBox.Show("Please enter the correct number of digtis", "CVC Verification 3 digits");
                txtCVCnumber.Focus();
            }
            else if(!int.TryParse(txtCVCnumber.Text, out cvcNumber))
            {
                MessageBox.Show("Please make sure you enter only numbers","CVC number erro: non-numbers");
                txtCVCnumber.Focus();
            }
            else
            {
                DialogResult result = MessageBox.Show("Are entries correct?", "Card Payment", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    saveReceipt();
                    MessageBox.Show("Thank you for dining with us " + userName);
                    txtCreditDebitCardNumber.Enabled = false;
                    txtCardExpiration.Enabled = false;
                    txtCVCnumber.Enabled = false;
                    btnEnter.Enabled = false;
                    Application.Restart(); //To restart program for new customer
                }
            }
        }

        
        private void frmCloseTab_Load(object sender, EventArgs e)
        {
            // Shrink form to hide components until needed
            Size compact = new Size(377, 474);
            this.Size = compact;

            // Check if order count file exists
            if (File.Exists(ORDER_COUNT_FILE)){
                sr = new StreamReader(ORDER_COUNT_FILE);
                orderNo = int.Parse(sr.ReadLine()) +1;
                sr.Close();
            }
            else // If not, create and initialize one
            {
                sw = File.CreateText(ORDER_COUNT_FILE);
                sw.WriteLine("1");
                orderNo = 1;
                sw.Close();
            }            

            printReceipt();   
        }

        public void printReceipt()
        {
            subtotal = 0;
            total = 0;
            tax = 0;
            receiptText = "";
            int paddingItem = 43;
            int paddingPrice = 8;           

            receiptText += "".PadRight(15) + "Delicioso E-Ristorante" + "\r\n";
            receiptText += "".PadRight(8) + "1234 Main St, Los Angeles, CA 90001" + "\r\n";
            receiptText += "Order No: " + orderNo + "\r\n";
            receiptText += "Customer: " + userName + "\r\n";
            receiptText += dateTime + "\r\n"; 
            receiptText += "---------------------------------------------------" + "\r\n";

            foreach (Item i in orderList)
            {
                subtotal += i.getPrice();
                receiptText += i.getName().PadRight(paddingItem) + i.getPrice().ToString("C").PadLeft(paddingPrice) + "\r\n";

                if (i.getNote().Length > 0)
                {
                    string[] tokens = i.getNote().Split(',');
                    foreach (string s in tokens)
                    {
                        receiptText += "     - " + s.Trim() + "\r\n";
                    }                    
                }
            }
            
            tax = subtotal * TAX_RATE;
            total = subtotal + tax + tip; 

            receiptText += "---------------------------------------------------" + "\r\n";
            receiptText += "Subtotal: ".PadRight(paddingItem) + subtotal.ToString("C").PadLeft(paddingPrice) + "\r\n";
            receiptText += "Tax: ".PadRight(paddingItem) + tax.ToString("C").PadLeft(paddingPrice) + "\r\n";
            receiptText += "Tip: ".PadRight(paddingItem) + tip.ToString("C").PadLeft(paddingPrice) + "\r\n";
            receiptText += "Total: ".PadRight(paddingItem) + total.ToString("C").PadLeft(paddingPrice);

            txtCloseTabTotal.Text = receiptText;
        }

        // Finalize order. Saves receipt in a text file named after the order#
        public void saveReceipt()
        {
            dateTime = DateTime.Now.ToString(); // Time stamp only displays when order is complete.
            printReceipt(); // refresh receipt with current time stamp at checkout

            if (radCash.Checked)
            {
                receiptText += "\r\n" + "".PadRight(37) + "Paid with Cash";
            }
            else
            {
                receiptText += "\r\n" + "".PadRight(37) + "Paid with Card";
            }

            sw = File.CreateText("."+RECEIPT_DIR + (orderNo) +".txt");
            sw.Write(receiptText);
            sw.Close();
            sw = File.CreateText(ORDER_COUNT_FILE);
            sw.WriteLine(orderNo);
            sw.Close();
        }
    }
}
