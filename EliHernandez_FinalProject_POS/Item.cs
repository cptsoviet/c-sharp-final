﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace EliHernandez_FinalProject_POS
{
    public class Item
    {
        string name = "";
        decimal price = 0;
        string description ="";
        string note = ""; // No pickles, extra tomatoes.. etc

        Image itemPic;

        public Item()
        {
            note = "";
        }

        public Item(string n, decimal p)
        {
            name = n;
            price = p;
            note = "";
        }

        // Copy constructor
        public Item(Item i)
        {
            this.name = i.name;
            this.price = i.price;
            this.description = i.description;
            this.note = i.note;
            this.itemPic = i.itemPic;
        }

        public Item(string n, decimal p, string d, string imgPath)
        {
            name = n;
            price = p;
            description = d;
            note = "";
            itemPic = Image.FromFile(imgPath);
        }

        public string getName() { return name; }

        public string getDescription() { return description; }

        public decimal getPrice() { return price; }

        public string getNote() { return note; }

        public Image getImage() { return itemPic; }

        public void setName(string n) { name = n; }

        public void setDescription(string d) { description = d; }

        public void setPrice(decimal p) { price = p; }

        public void setNote(string n) { note = n; }        

        public void setImage(string path) { itemPic = Image.FromFile(path); }

        // Override ToString method, this is what will be displayed when an item is in a listbox.
        public override string ToString() { return this.name + " - " + this.price.ToString("c"); }
    }
}
