               Delicioso E-Ristorante
        1234 Main St, Los Angeles, CA 90001
Order No: 7
Customer: sfg
5/21/2018 5:09:03 AM
---------------------------------------------------
Spaghettini with Meatballs                   $15.99
Sausage Tortelloni                           $16.99
Pasta Woozie                                 $15.99
     - Add tomaters. Potaters
Crispy Fish Sandwich                         $12.99
---------------------------------------------------
Subtotal:                                    $61.96
Tax:                                          $5.89
Tip:                                          $6.66
Total:                                       $74.51
                                     Paid with Card