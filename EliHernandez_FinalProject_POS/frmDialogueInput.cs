﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EliHernandez_FinalProject_POS
{
    public partial class frmDialogueInput : Form
    {
        string header;
        string label;

        string userInput;

        public frmDialogueInput(string header, string label)
        {
            this.header = header;
            this.label = label;
            InitializeComponent();
        }

        private void frmDialogueInput_Load(object sender, EventArgs e)
        {
            this.Text = header;
            lblDisplayText.Text = label;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {            
            DialogResult result = MessageBox.Show("Is " + txtInput.Text + " correct?", "Customer Name", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                userInput = txtInput.Text;
                Close();
            }               
        }

        public string getuserInput()
        {
            return userInput;
        }
    }
}
