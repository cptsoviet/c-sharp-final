﻿/*
 Serge
 Fabian
 Eli

 C# Final Lab
 Restaurant POS
 
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.ObjectModel;

namespace EliHernandez_FinalProject_POS
{
    public partial class frmMain : Form
    {
        frmSplash splash = new frmSplash();
        //create instance of payment screen
        private frmCloseTab paymentOption;
        public string customerName = "";

        const decimal TAX_RATE = 0.95m; // move to checkout form?
        
        // four lists containing restaurant menu items
        List<Item> entreeMenu = new List<Item>();
        List<Item> saladMenu = new List<Item>();
        List<Item> drinkMenu = new List<Item>();
        List<Item> dessertMenu = new List<Item>();
        
        // Contains customer order
        List<Item> orderList = new List<Item>();

        // This function adds menu items to appropriate arrays
        public void populateMenu()
        {
            // Entrees
            entreeMenu.Add(new Item("Pasta Delicioso", 15.99m, "grilled chicken, mushrooms, rigatoni, roasted red pepper cream", "MenuPics/PastaDelicioso.jpg"));
            entreeMenu.Add(new Item("Pasta Woozie", 15.99m, "grilled chicken, fettuccine, alfredo, spinach", "MenuPics/PastaWoozie.jpg"));
            entreeMenu.Add(new Item("Sausage Tortelloni", 16.99m, "pancetta, pomodoro, chile flake, mozzarella, basil", "MenuPics/SausageTortelloni.jpg"));
            entreeMenu.Add(new Item("Lobster Ravioli Alla Vodka", 18.99m, "sautéed shrimp, tomato cream, pesto bread crumbs", "MenuPics/LobsterRavioliAllaVodka.jpg"));
            entreeMenu.Add(new Item("Spaghettini with Meatballs", 15.99m, "house-made beef and pork meatballs, marinara", "MenuPics/SpaghettiniMeatballs.jpg"));
            entreeMenu.Add(new Item("Pepperoni Classica Pizza", 12.29m, "roasted banana peppers, mozzarella, oregano", "MenuPics/PepperoniClassicaPizza.jpg"));
            entreeMenu.Add(new Item("Margherita Pizza", 11.49m, "tomatoes, mozzarella, basil", "MenuPics/MargheritaPizza.jpeg"));
            entreeMenu.Add(new Item("Roasted Turkey + Bacon Sandwich", 11.79m, "tomatoes, arugula, green onion mayo, avocado aioli, toasted brioche, fresh fruit", "MenuPics/RoastedTurkeyBaconSandwich.jpeg"));
            entreeMenu.Add(new Item("Grilled Chicken + Candied Bacon Sandwich", 11.49m, "whole wheat bun, cilantro aioli, arugula, tomatoes, green onion mayo, sweet potato fries", "MenuPics/GrilledChickenCandiedBaconSandwich.jpeg"));
            entreeMenu.Add(new Item("Crispy Fish Sandwich", 12.99m, "cod, brussels sprouts slaw, sriracha honey, tuscan fries, whole wheat bun", "MenuPics/CrispyFishSandwich.jpg"));
            entreeMenu.Add(new Item("Bacon Cheeseburger", 13.99m, "applewood bacon, provolone, lettuce, tomato, mayo tuscan fries, whole wheat bun", "MenuPics/BaconCheeseburger.jpg"));

            // Salads
            saladMenu.Add(new Item("Mediterranean Veg Salad", 11.99m, "grilled vegetables, tomatoes, orzo, farro, mixed greens, sweet garlic vinaigrette, pine nuts,\nfeta, balsamic drizzle", "MenuPics/MediterraneanVegSalad.jpg"));
            saladMenu.Add(new Item("Insalata Della Casa", 5.99m, "chopped greens, cucumbers, tomatoes, bacon, crispy pasta, parmesan dressing", "MenuPics/InsalataDellaCasa.jpg"));
            saladMenu.Add(new Item("Delicioso Chopped Salad", 5.99m, "chopped greens, cucumbers, red onions, tomatoes, olives, feta, red wine vinaigrette", "MenuPics/ChoppedSalad.jpg"));
            saladMenu.Add(new Item("Ceasar Classica", 5.99m, "focaccia croutons", "MenuPics/CaesarClassica.jpg"));
            saladMenu.Add(new Item("Grilled Chicken Chopped Salad", 13.99m, "gorgonzola, pecans, crispy pasta, herb vinaigrette", "MenuPics/GrilledChickenChoppedSalad.jpg"));
            
            // Drinks
            // Drinks -> Red Wine
            drinkMenu.Add(new Item("Bottle - Sartpro Family Reserve", 30.00m, "Verona, Italy\nPinot Noir", "MenuPics/SartoriFamilyReserve.jpg"));
            drinkMenu.Add(new Item("Glass - Sartpro Family Reserve", 7.50m, "Verona, Italy\nPinot Noir", "MenuPics/SartoriFamilyReserve.jpg"));
            drinkMenu.Add(new Item("Bottle - Sketchbook", 40m, "Mendocino, California\nPinot Noir", "MenuPics/Sketchbook.jpg"));
            drinkMenu.Add(new Item("Glass - Sketchbook", 10m, "Mendocino, California\nPinot Noir", "MenuPics/Sketchbook.jpg"));
            drinkMenu.Add(new Item("Bottle - Placido Chinati D.O.C.G", 32m, "Tuscani, Italy\nChianti", "MenuPics/PlacidoChiantiDOCG.jpg"));
            drinkMenu.Add(new Item("Glass - Placido Chinati D.O.C.G", 8m, "Tuscani, Italy\nChianti", "MenuPics/PlacidoChiantiDOCG.jpg"));
            drinkMenu.Add(new Item("Bottle - Rocca Delle Macie Chianti Classico", 42m, "Tuscani, Italy\nChianti", "MenuPics/RoccaDelleMacieChiantiClassico.jpg"));
            drinkMenu.Add(new Item("Glass - Rocca Delle Macie Chianti Classico", 10.5m, "Tuscani, Italy\nChianti", "MenuPics/RoccaDelleMacieChiantiClassico.jpg"));
            // Drinks -> White Wine
            drinkMenu.Add(new Item("Bottle - Caposaldo", 34.00m, "Lombardy, Italy\nMoscato", "MenuPics/Caposaldo.jpg"));
            drinkMenu.Add(new Item("Glass - Caposaldo", 8.50m, "Lombardy, Italy\nMoscato", "MenuPics/Caposaldo.jpg"));
            drinkMenu.Add(new Item("Bottle - Wente Vineyards Riverbank", 30m, "Monterey, California\nRiesling", "MenuPics/WenteVineyardsRiverbank.jpeg"));
            drinkMenu.Add(new Item("Glass - Wente Vineyards Riverbank", 7.50m, "Monterey, California\nRiesling", "MenuPics/WenteVineyardsRiverbank.jpeg"));
            drinkMenu.Add(new Item("Bottle - Acrobat", 40m, "Oregon\nPinot Gris", "MenuPics/Acrobat.jpg"));
            drinkMenu.Add(new Item("Glass - Acrobat", 10m, "Oregon\nPinot Gris", "MenuPics/Acrobat.jpg"));
            drinkMenu.Add(new Item("Bottle - Sartori Family Reserve", 32m, "Veneto, Italy\nPinot Grigio", "MenuPics/SartoriFamilyReservePinotGrigio .jpeg"));
            drinkMenu.Add(new Item("Glass - Sartori Family Reserve", 8m, "Veneto, Italy\nPinot Grigio", "MenuPics/SartoriFamilyReservePinotGrigio .jpeg"));

            // Desserts
            dessertMenu.Add(new Item("Tiramisu", 6.99m, "lady fingers soaked in coffee liqueur, creamy mascarpone filling, cocoa powder", "MenuPics/Tiramisu.jpg"));
            dessertMenu.Add(new Item("Warm Chocolate Cake", 6.99m, "rich chocolate cake, molten lava center, vanilla bean gelato", "MenuPics/WarmChocolateCake.jpg"));
            dessertMenu.Add(new Item("Creme Brulee", 5.99m, "vanilla bean custard, caramelized sugar crust", "MenuPics/CremeBrulee.jpg"));
            dessertMenu.Add(new Item("Caramel Mascarpone Cheesecake", 6.99m, "mascarpone cream, anglaise, caramel drizzle", "MenuPics/CaramelMascarponeCheesecake.jpg"));
        }

        public frmMain()
        {
            InitializeComponent();
        }

        // Open Checkout window
        private void btnTotal_Click(object sender, EventArgs e)
        {
            //to slow down user error input secondary authentication
            DialogResult result = MessageBox.Show("Are you sure you want to close the tab?", "Close Order", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                paymentOption = new frmCloseTab(customerName, orderList);
                paymentOption.ShowDialog();
            }
        }

        // Restart form
        private void btnReset_Click(object sender, EventArgs e)
        {            
            DialogResult result = MessageBox.Show("Are you sure you want to restart order for " + customerName +"?", "Reset Order", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                Application.Restart();
            }
        }

        //***ADD***
        private void btnAdd_Click(object sender, EventArgs e)
        {
            addItem();
        }

        // copy food item from menu array to order array.
        private void lstMenuItems_DoubleClick(object sender, EventArgs e)
        {
            addItem();
        }

        //***EDIT***
        private void btnEdit_Click(object sender, EventArgs e)
        {
            //enable/disable certain buttons based on selection
            btnAddNote.Enabled = true;
            btnDelete.Enabled = true;
            btnCancel.Enabled = true;
            btnCancel.Visible = true;
            btnAdd.Enabled = false;
            btnEntree.Enabled = false;
            btnSalads.Enabled = false;
            btnDrinks.Enabled = false;
            btnDesserts.Enabled = false;
            btnTotal.Enabled = false;
            lstMenuItems.Enabled = false;
            lstReceipt.Enabled = true;
        }

        // Cancels "Edit Order" mode
        private void btnCancel_Click(object sender, EventArgs e)
        {            
            //enable dishes
            btnEntree.Enabled = true;
            btnSalads.Enabled = true;
            btnDrinks.Enabled = true;
            btnDesserts.Enabled = true;
            btnAddNote.Enabled = false;
            //enable menu list ,disable receipt list
            lstMenuItems.Enabled = true;
            lstReceipt.Enabled = false;

            //enable add and close tab button,disable delete
            btnAdd.Enabled = true;
            btnTotal.Enabled = true;
            btnDelete.Enabled = false;
            btnCancel.Visible = false;
            btnCancel.Enabled = false;
        }

        // Delete item from order list
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lstReceipt.SelectedIndex != -1)
            {
                Item selected = (Item)lstReceipt.SelectedItem;

                // Show deletion confirmation dialog
                DialogResult result = MessageBox.Show("Are you sure you want to\n" +
                   "delete "+ selected.getName() + "?\n", "Delete Item", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    orderList.Remove(selected);
                    updateReceipt();
                }
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            // Show Splash
            splash.ShowDialog();
            
            // Initialize input box dialog
            frmDialogueInput f = new frmDialogueInput("Name", "Please enter your name: ");
            f.ShowDialog(); // show modal

            // get the user input string
            customerName = f.getuserInput();

            // Initialize Tooltips
            toolTip.SetToolTip(this.btnReset, "Restart order process.");
            toolTip.SetToolTip(this.btnAdd, "Add Item to your order.");
            toolTip.SetToolTip(this.btnEdit, "Edit item in your order.");
            toolTip.SetToolTip(this.btnCancel, "Stop Editing");
            toolTip.SetToolTip(this.btnTotal, "Place your order");

            // Disable menu list until user selects menu type (Entree, Salds, etc) and display instructions in listbox.
            lstMenuItems.Enabled = false;
            lstMenuItems.Items.Add("Hello, " + customerName + ", please select one of the");
            lstMenuItems.Items.Add("menu options above to see the menu items.");

            populateMenu();
        }

        private void btnEntree_Click(object sender, EventArgs e)
        {
            // Datasource property clones (shallow copy) an entire array into listbox.
            lstMenuItems.DataSource = entreeMenu;

            enableControlsOnSubmenuClick();
        }

        private void btnSalads_Click(object sender, EventArgs e)
        {
            // Datasource property clones (shallow copy) an entire array into listbox.
            lstMenuItems.DataSource = saladMenu;

            enableControlsOnSubmenuClick();
        }

        private void btnDrinks_Click(object sender, EventArgs e)
        {
            // Datasource property clones (shallow copy) an entire array into listbox.
            lstMenuItems.DataSource = drinkMenu;

            enableControlsOnSubmenuClick();
        }

        private void btnDesserts_Click(object sender, EventArgs e)
        {
            // Datasource property clones (shallow copy) an entire array into listbox.
            lstMenuItems.DataSource = dessertMenu;

            enableControlsOnSubmenuClick();
        }

        // This function is called after adding a new item, it updates the order listbox
        public void updateReceipt()
        {
            // Clear listbox
            lstReceipt.Items.Clear();

            // Iterate through order array and populate listbox
            foreach (Item i in orderList)
            {
                lstReceipt.Items.Add(i);
                
                // if item contains a notes, add them
                if (i.getNote().Length > 0)
                {
                    string[] tokens = i.getNote().Split(',');

                    foreach(string s in tokens)
                    {
                        lstReceipt.Items.Add("     - " + s.Trim());
                    }                    
                }
            }
        }
        
        private void btnAddNote_Click(object sender, EventArgs e)
        {
            // Initialize input dialog box
            frmDialogueInput f = new frmDialogueInput("Note", "Please enter a note to add to selcted item (Separated by commas):");

            // Determine which item is selected            
            Item selected = (Item)lstReceipt.SelectedItem;

            f.ShowDialog(); // Show dialog box as modal
            string note = f.getuserInput(); // Get user input
                        
            int index = orderList.IndexOf(selected);
            orderList[index].setNote(note);
                
            updateReceipt();            
        }

        // Displays images and food description when selecting items from the menu
        private void lstMenuItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            Item selected = (Item)lstMenuItems.SelectedItem;
            picMenuItem.Image = selected.getImage();
            lblDescription.Text = selected.getName() + ": " + "\n      -" + selected.getDescription();
        }

        void enableControlsOnSubmenuClick()
        {
            // Enable/disable appropriate controls
            btnAdd.Enabled = true;
            lstMenuItems.Enabled = true;
            lstReceipt.Enabled = false;
            lblMenuDescription.Visible = true;
            picMenuItem.Visible = true;
            lblDescription.Visible = true;
        }

        // Copy selected item from menu to order list 
        void addItem()
        {
            Item i = (Item)lstMenuItems.SelectedItem;
            orderList.Add(new Item(i));

            updateReceipt();

            btnEdit.Enabled = true;
            btnTotal.Enabled = true;
        }
    }
}
