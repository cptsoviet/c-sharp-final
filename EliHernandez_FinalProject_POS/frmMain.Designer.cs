﻿namespace EliHernandez_FinalProject_POS
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.label1 = new System.Windows.Forms.Label();
            this.gbxReceipt = new System.Windows.Forms.GroupBox();
            this.lstReceipt = new System.Windows.Forms.ListBox();
            this.picMenuItem = new System.Windows.Forms.PictureBox();
            this.lstMenuItems = new System.Windows.Forms.ListBox();
            this.gbxMenuItems = new System.Windows.Forms.GroupBox();
            this.btnTotal = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblMenuDescription = new System.Windows.Forms.Label();
            this.lblReceipt = new System.Windows.Forms.Label();
            this.btnEntree = new System.Windows.Forms.Button();
            this.btnSalads = new System.Windows.Forms.Button();
            this.btnDesserts = new System.Windows.Forms.Button();
            this.btnAddNote = new System.Windows.Forms.Button();
            this.lblDescription = new System.Windows.Forms.Label();
            this.btnDrinks = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.gbxReceipt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMenuItem)).BeginInit();
            this.gbxMenuItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.Location = new System.Drawing.Point(11, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(339, 124);
            this.label1.TabIndex = 0;
            this.label1.Text = "Delicioso E-Ristorante";
            // 
            // gbxReceipt
            // 
            this.gbxReceipt.BackColor = System.Drawing.Color.DimGray;
            this.gbxReceipt.Controls.Add(this.lstReceipt);
            this.gbxReceipt.Location = new System.Drawing.Point(1544, 336);
            this.gbxReceipt.Margin = new System.Windows.Forms.Padding(6);
            this.gbxReceipt.Name = "gbxReceipt";
            this.gbxReceipt.Padding = new System.Windows.Forms.Padding(6);
            this.gbxReceipt.Size = new System.Drawing.Size(486, 609);
            this.gbxReceipt.TabIndex = 6;
            this.gbxReceipt.TabStop = false;
            // 
            // lstReceipt
            // 
            this.lstReceipt.FormattingEnabled = true;
            this.lstReceipt.ItemHeight = 24;
            this.lstReceipt.Location = new System.Drawing.Point(11, 15);
            this.lstReceipt.Margin = new System.Windows.Forms.Padding(6);
            this.lstReceipt.Name = "lstReceipt";
            this.lstReceipt.Size = new System.Drawing.Size(461, 580);
            this.lstReceipt.TabIndex = 8;
            // 
            // picMenuItem
            // 
            this.picMenuItem.Location = new System.Drawing.Point(589, 229);
            this.picMenuItem.Margin = new System.Windows.Forms.Padding(6);
            this.picMenuItem.Name = "picMenuItem";
            this.picMenuItem.Size = new System.Drawing.Size(871, 510);
            this.picMenuItem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMenuItem.TabIndex = 7;
            this.picMenuItem.TabStop = false;
            this.picMenuItem.Visible = false;
            // 
            // lstMenuItems
            // 
            this.lstMenuItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.142858F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstMenuItems.FormattingEnabled = true;
            this.lstMenuItems.ItemHeight = 24;
            this.lstMenuItems.Location = new System.Drawing.Point(11, 15);
            this.lstMenuItems.Margin = new System.Windows.Forms.Padding(6);
            this.lstMenuItems.Name = "lstMenuItems";
            this.lstMenuItems.Size = new System.Drawing.Size(461, 580);
            this.lstMenuItems.TabIndex = 7;
            this.lstMenuItems.SelectedIndexChanged += new System.EventHandler(this.lstMenuItems_SelectedIndexChanged);
            this.lstMenuItems.DoubleClick += new System.EventHandler(this.lstMenuItems_DoubleClick);
            // 
            // gbxMenuItems
            // 
            this.gbxMenuItems.BackColor = System.Drawing.Color.DimGray;
            this.gbxMenuItems.Controls.Add(this.lstMenuItems);
            this.gbxMenuItems.Location = new System.Drawing.Point(22, 336);
            this.gbxMenuItems.Margin = new System.Windows.Forms.Padding(6);
            this.gbxMenuItems.Name = "gbxMenuItems";
            this.gbxMenuItems.Padding = new System.Windows.Forms.Padding(6);
            this.gbxMenuItems.Size = new System.Drawing.Size(486, 609);
            this.gbxMenuItems.TabIndex = 5;
            this.gbxMenuItems.TabStop = false;
            // 
            // btnTotal
            // 
            this.btnTotal.BackColor = System.Drawing.Color.Silver;
            this.btnTotal.Enabled = false;
            this.btnTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotal.ForeColor = System.Drawing.Color.Black;
            this.btnTotal.Location = new System.Drawing.Point(1274, 858);
            this.btnTotal.Margin = new System.Windows.Forms.Padding(6);
            this.btnTotal.Name = "btnTotal";
            this.btnTotal.Size = new System.Drawing.Size(185, 87);
            this.btnTotal.TabIndex = 8;
            this.btnTotal.Text = "Close Tab";
            this.btnTotal.UseVisualStyleBackColor = false;
            this.btnTotal.Click += new System.EventHandler(this.btnTotal_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.Silver;
            this.btnEdit.Enabled = false;
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ForeColor = System.Drawing.Color.Black;
            this.btnEdit.Location = new System.Drawing.Point(1032, 858);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(6);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(185, 87);
            this.btnEdit.TabIndex = 9;
            this.btnEdit.Text = "Edit Order";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Silver;
            this.btnAdd.Enabled = false;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.Black;
            this.btnAdd.Location = new System.Drawing.Point(809, 858);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(6);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(185, 87);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.Silver;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.Black;
            this.btnReset.Location = new System.Drawing.Point(589, 858);
            this.btnReset.Margin = new System.Windows.Forms.Padding(6);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(185, 87);
            this.btnReset.TabIndex = 11;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Silver;
            this.btnDelete.Enabled = false;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(1892, 253);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(6);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(138, 72);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Silver;
            this.btnCancel.Enabled = false;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.Black;
            this.btnCancel.Location = new System.Drawing.Point(1032, 858);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(185, 87);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblMenuDescription
            // 
            this.lblMenuDescription.AutoSize = true;
            this.lblMenuDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMenuDescription.Location = new System.Drawing.Point(194, 264);
            this.lblMenuDescription.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblMenuDescription.Name = "lblMenuDescription";
            this.lblMenuDescription.Size = new System.Drawing.Size(77, 29);
            this.lblMenuDescription.TabIndex = 8;
            this.lblMenuDescription.Text = "Menu";
            // 
            // lblReceipt
            // 
            this.lblReceipt.AutoSize = true;
            this.lblReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceipt.Location = new System.Drawing.Point(1700, 273);
            this.lblReceipt.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblReceipt.Name = "lblReceipt";
            this.lblReceipt.Size = new System.Drawing.Size(150, 29);
            this.lblReceipt.TabIndex = 14;
            this.lblReceipt.Text = "Your Order:";
            // 
            // btnEntree
            // 
            this.btnEntree.BackColor = System.Drawing.Color.White;
            this.btnEntree.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEntree.BackgroundImage")));
            this.btnEntree.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnEntree.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEntree.Location = new System.Drawing.Point(339, 17);
            this.btnEntree.Margin = new System.Windows.Forms.Padding(6);
            this.btnEntree.Name = "btnEntree";
            this.btnEntree.Size = new System.Drawing.Size(336, 170);
            this.btnEntree.TabIndex = 22;
            this.btnEntree.Text = "Entrée";
            this.btnEntree.UseVisualStyleBackColor = false;
            this.btnEntree.Click += new System.EventHandler(this.btnEntree_Click);
            // 
            // btnSalads
            // 
            this.btnSalads.BackColor = System.Drawing.Color.White;
            this.btnSalads.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSalads.BackgroundImage")));
            this.btnSalads.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSalads.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalads.Location = new System.Drawing.Point(686, 17);
            this.btnSalads.Margin = new System.Windows.Forms.Padding(6);
            this.btnSalads.Name = "btnSalads";
            this.btnSalads.Size = new System.Drawing.Size(336, 170);
            this.btnSalads.TabIndex = 23;
            this.btnSalads.Text = "Salads";
            this.btnSalads.UseVisualStyleBackColor = false;
            this.btnSalads.Click += new System.EventHandler(this.btnSalads_Click);
            // 
            // btnDesserts
            // 
            this.btnDesserts.BackColor = System.Drawing.Color.White;
            this.btnDesserts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDesserts.BackgroundImage")));
            this.btnDesserts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnDesserts.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesserts.Location = new System.Drawing.Point(1379, 17);
            this.btnDesserts.Margin = new System.Windows.Forms.Padding(6);
            this.btnDesserts.Name = "btnDesserts";
            this.btnDesserts.Size = new System.Drawing.Size(336, 170);
            this.btnDesserts.TabIndex = 25;
            this.btnDesserts.Text = "Desserts";
            this.btnDesserts.UseVisualStyleBackColor = false;
            this.btnDesserts.Click += new System.EventHandler(this.btnDesserts_Click);
            // 
            // btnAddNote
            // 
            this.btnAddNote.Enabled = false;
            this.btnAddNote.Location = new System.Drawing.Point(1544, 253);
            this.btnAddNote.Margin = new System.Windows.Forms.Padding(6);
            this.btnAddNote.Name = "btnAddNote";
            this.btnAddNote.Size = new System.Drawing.Size(138, 72);
            this.btnAddNote.TabIndex = 26;
            this.btnAddNote.Text = "Note";
            this.btnAddNote.UseVisualStyleBackColor = true;
            this.btnAddNote.Click += new System.EventHandler(this.btnAddNote_Click);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblDescription.Location = new System.Drawing.Point(581, 744);
            this.lblDescription.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(238, 25);
            this.lblDescription.TabIndex = 27;
            this.lblDescription.Text = "Food name, ingredients";
            this.lblDescription.Visible = false;
            // 
            // btnDrinks
            // 
            this.btnDrinks.BackColor = System.Drawing.Color.White;
            this.btnDrinks.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDrinks.BackgroundImage")));
            this.btnDrinks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnDrinks.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDrinks.Location = new System.Drawing.Point(1032, 17);
            this.btnDrinks.Margin = new System.Windows.Forms.Padding(6);
            this.btnDrinks.Name = "btnDrinks";
            this.btnDrinks.Size = new System.Drawing.Size(336, 170);
            this.btnDrinks.TabIndex = 24;
            this.btnDrinks.Text = "Drinks";
            this.btnDrinks.UseVisualStyleBackColor = false;
            this.btnDrinks.Click += new System.EventHandler(this.btnDrinks_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(2052, 982);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.btnAddNote);
            this.Controls.Add(this.btnDesserts);
            this.Controls.Add(this.btnDrinks);
            this.Controls.Add(this.btnSalads);
            this.Controls.Add(this.btnEntree);
            this.Controls.Add(this.lblReceipt);
            this.Controls.Add(this.lblMenuDescription);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnTotal);
            this.Controls.Add(this.picMenuItem);
            this.Controls.Add(this.gbxReceipt);
            this.Controls.Add(this.gbxMenuItems);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "frmMain";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.gbxReceipt.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picMenuItem)).EndInit();
            this.gbxMenuItems.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbxReceipt;
        private System.Windows.Forms.ListBox lstReceipt;
        private System.Windows.Forms.PictureBox picMenuItem;
        private System.Windows.Forms.ListBox lstMenuItems;
        private System.Windows.Forms.GroupBox gbxMenuItems;
        private System.Windows.Forms.Button btnTotal;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblMenuDescription;
        private System.Windows.Forms.Label lblReceipt;
        private System.Windows.Forms.Button btnEntree;
        private System.Windows.Forms.Button btnSalads;
        private System.Windows.Forms.Button btnDesserts;
        private System.Windows.Forms.Button btnAddNote;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Button btnDrinks;
        private System.Windows.Forms.ToolTip toolTip;
    }
}

